import React, {Component, Fragment} from 'react';
import './Blog.css';
import Post from "../../components/Post/Post";

class Blog extends Component {
  state = {
    posts: [],
    postsFormShown: false
  };

  togglePostsForm = () => {
    this.setState(prevState => {
      return {postsFormShown: !prevState.postsFormShown};
    });
  };

  componentDidMount() {
    const P_URL = 'https://jsonplaceholder.typicode.com/posts?_limit=4';
    fetch(P_URL).then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Something went wrong with network request');
    }).then(posts => {
      const updatedPosts = posts.map(post => {
        return {
          ...post,
          author: 'John Doe'
        }
      });

      this.setState({posts: updatedPosts});
    }).catch(error => {
      console.log(error);
    });
  }

  render() {
    let postsForm = null;

    if (this.state.postsFormShown) {
      postsForm = (
        <section className="NewPost">
          <p>New post form will be here</p>
        </section>
      );
    }

    return (
      <Fragment>
        <section className="Posts">
          {this.state.posts.map(post => (
            <Post key={post.id} title={post.title} author={post.author} />
          ))}
        </section>
        <button className="ToggleButton" onClick={this.togglePostsForm}>
          New Post
        </button>
        {postsForm}
      </Fragment>
    );
  }
}

export default Blog;